# 运行手册

## 本地运行
### 数据库
1. 初始化，Navicat直接执行：
    D:\Workspaces\IDEA\Opensource\oa_system\oasys.sql
2. 配置数据库连接
    D:\Workspaces\IDEA\Opensource\oa_system\src\main\resources\application.properties
3. 更新jdbc版本 (5->8)
    D:\Workspaces\IDEA\Opensource\oa_system\pom.xml

### 后端
1. Run cn.gson.oasys.OasysApplication#main()
2. 登录验证
http://127.0.0.1:8088/index
    soli\123456
